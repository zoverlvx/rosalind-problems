#!/bin/bash

months=${1:-5}
offspring=${2:-3}

child=1
fertile_pair=1
for (( i=months-1; i > 0; i-- )); do
  produced=$(( child * offspring ))
  child=$fertile_pair
  fertile_pair=$(( fertile_pair + produced ))
done

echo $child
