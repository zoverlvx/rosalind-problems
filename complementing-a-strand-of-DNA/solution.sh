#!/bin/bash

DNAseq=${1:-"AAAACCCGGT"} # take from stdin or default
DNAseq=${DNAseq^^} # change all to uppercase

len=${#DNAseq} # get length of sequence

declare -A complement=( ["A"]="T" ["T"]="A" ["C"]="G" ["G"]="C" )

translated=""

# start from end of sequence
for (( i=$len - 1; i >= 0; i--)); do
  char="${DNAseq:$i:1}" # get character
  translated+="${complement[$char]}" # get complement and concat to string
done

echo "$translated" # and reversed
