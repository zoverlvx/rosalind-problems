const readline = require("readline");
const fs = require("fs");
const file = "./rosalind.fasta"

const r1 = readline.createInterface({
    input: fs.createReadStream(file),
    output: process.stdout,
    terminal: false
});

const sequences = new Map();
const label = [];
let counter = 0;

/*
console.time("string storage")
r1.on("line", function (line) {
    if (line[0] === ">") {
	label.pop();
	const labelName = line.slice(1)
        sequences.set(labelName, "")
	label.push(labelName)
    }
    if (line[0] !== ">") {
        const value = sequences.get(label[0]);
	sequences.set(label[0], value + line)
    }
    counter+=1;
    if (counter === 3) {
        const value = sequences.get(label[0]);
	const len = value.length;
	const GCs = value.match(/[GC]/g).length;
	console.log(label[0]);
	console.log((GCs/len*100).toFixed(6));
	counter = 0;
    }
});
console.timeEnd("string storage")
*/

// fastest
console.time("integer storage")
r1.on("line", function (line) {
    // if the first char of the line is >
    // this will be the label of the sequence
    if (line[0] === ">") {
	// throw out old label
	label.pop();
	const labelName = line.slice(1) // remove > from line
        sequences.set(labelName, [0, 0])
	label.push(labelName) // store label for later use i.e. Rosalind_0808
    }
    // if it's not a label, it's a sequence
    if (line[0] !== ">") {
	// totalLen = total length of the DNA sequence so far
	// totalGCs = total GCs found in the DNA sequence so far
        const [totalLen, totalGCs] = sequences.get(label[0]);
	const len = line.length; // additional length of total sequence
        // additional count of total GCs in sequence
	const GCs = line.match(/[GC]/g).length;
	// add the additional length of the total sequence to what has been found
	    // add the additional count of GCs found in sequence to what has already been found prior in this sequence
	sequences.set(label[0], [totalLen + len, totalGCs + GCs]);
    }
    counter+=1;
    if (counter === 3) {
        const [completeSequenceLen, GCs]= sequences.get(label[0]);
	console.log(label[0]);
	console.log((GCs/completeSequenceLen*100).toFixed(6));
	counter = 0;
    }
});
console.timeEnd("integer storage")

