declare -A a=()
end=0
while read line; do
  # set label as key with empty value
  [ "${line:0:1}" == ">" ] && label="${line:1}" && a+=(["$label"]="")
  # concat the sequence to the value of the key
  [ "${line:0:1}" != ">" ] && a["$label"]+="$line"
  (( end++ ))
  # if we've collected all of the sequence  
  if [ $end -eq 3 ]; then
    # find the length of the sequence
    len="${#a["$label"]}"
    seq="${a["$label"]}"
    # find the GC count of the sequence
    GC_count="$(sed -e "s/./&\n /g" <<< "$seq" | grep -c "[CG]")"
    # find the percentage of the sequence
    percent="$(awk -vn=$GC_count -vl=$len 'BEGIN{print(n/l*100)}')"
    # remove old value with percent as new value
    a["$label"]="$percent"
    echo "$label"
    echo "$percent"
    end=0
  fi
done < "$1" # file.fasta

## if looking for a specific label
[ ! -z "$2" ] && echo "${a["$2"]}"

