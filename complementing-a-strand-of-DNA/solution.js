const DNAseq = process.argv[2] || "AAAACCCGGT".split("").reverse();

const complement = {"A": "T", "C": "G", "G": "C", "T": "A"}

let result = "";

DNAseq.forEach(char => result += complement[char]);

console.log(result);
