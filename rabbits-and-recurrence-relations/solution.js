const numOfMonths = Number(process.argv[2]) || 5;
const numOfOffspring = Number(process.argv[3]) || 3;

console.log(
    rabbitReproductionCount(
        numOfMonths,
	numOfOffspring
    )
);

function lifecycle(months, offspring, children, parents) {
    if (months > 0) {
	const newFertilePairs = parents + (children * offspring);
	// newFertilePairs: those mature last cycle + those matured this cycle
	return lifecycle(months - 1, offspring, parents, newFertilePairs);
    }
    return children;
}

function rabbitReproductionCount(months, offspring) {
    const fertilePairs = 1;
    const children = 1;
    const population = lifecycle(months - 1, offspring, children, fertilePairs);
    return population;
}

