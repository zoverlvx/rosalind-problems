/*
 * Problem
 *
 * A string is simply an ordered collection of symbols selected from some alphabet and formed into a word; the length of a string is the number of symbols that it contains.
 *
 * An example of a length 21 DNA string (whose alphabet contains the symbols 'A', 'C', 'G', and 'T') is "ATGCTTCAGAAAGGTCTTACG."
 *
 * Given: A DNA string s
 *
 * of length at most 1000 nt.
 *
 * Return: Four integers (separated by spaces) counting the respective number of times that the symbols 'A', 'C', 'G', and 'T' occur in s
 *
 * .
 * Sample Dataset
 *
 * AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC
 *
 * Sample Output
 *
 * 20 12 17 21
 *
 * */

const arg = process.argv[2] || "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"

function nucleotideCounts(
    string,
    i = 0,
    map = new Map(),
    len = string.length
) {
    
    if (i < len) {
        const nucleotide = string[i];
        const count = map.get(nucleotide);

        if (count) {
            map.set(nucleotide, count + 1);
	}
        if (!count) {
            map.set(nucleotide, 1);
	}
        return nucleotideCounts(string, i + 1, map);
    }
    const A = map.get("A");
    const C = map.get("C");
    const G = map.get("G");
    const T = map.get("T");
    return `${A} ${C} ${G} ${T}`;
}

const result = nucleotideCounts(arg);
console.log(result);


