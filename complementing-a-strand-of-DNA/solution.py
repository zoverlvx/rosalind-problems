## Get input or default to string
## capitalize input
DNAseq=(input("Enter sequence: \n") or "AAAACCCGGT").upper()
## reverse string
reversed=DNAseq[::-1]
## String => List[Char]
reversed_list=[char for char in reversed]
## Dictionary for DNA complements
complement={'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'} 
result=""

for char in reversed_list:
    result+=complement[char]


print(result)
