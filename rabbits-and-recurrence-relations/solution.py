number_of_months=int(input("Enter number of months: ") or 5)
number_of_offspring=int(input("Enter number of offspring: ") or 3)

def rabbit_reproduction_count(months, offspring_produceable):
    fertile_pair, child = 1, 1
    for itr in range(months - 1):
        child, fertile_pair = fertile_pair, fertile_pair + (child * offspring_produceable)
    return child
total_amount=rabbit_reproduction_count(number_of_months, number_of_offspring)
print(total_amount)

"""
E.g. rabbit_reproduction_count(6, 2)

o - small (children) rabbits. They have to mature and reproduce in the next cycle only.

0 - fertile (parents) rabbits. They can reproduce and move to the next cycle.


Month 1: [o]
Month 2: [0]
Month 3: [0 o o]
Month 4: [0 o o 0 0]
Month 5: [0 o o 0 0 0  o o 0 o o]
Month 6: [0 o o 0 0 0 o o 0 o o 0 o o 0 0 0 o o 0 0]
"""
